<?php

use App\Http\Controllers\Auth\PartnerAuthController;
use App\Http\Controllers\Auth\UserAuthController;
use App\Http\Controllers\ChatController;
use App\Http\Controllers\PartnerChatController;
use App\Http\Controllers\PartnerController;
use App\Http\Controllers\RevenueSummaryController;
use Illuminate\Support\Facades\Route;

/**
 * API Routes
 *
 * These routes are responsible for handling API requests related to the assistant and partner functionalities,
 * including authentication, partner management, chat functionalities, and revenue summaries.
 *
 * Routes within the 'assistant' prefix require authentication and ability checks for assistant role.
 * Routes within the 'partner' prefix require authentication and ability checks for partner role.
 *
 * @group Assistant
 * @group Partner
 *
 * @prefix api/assistant
 * @prefix api/partner
 *
 * @middleware auth:sanctum
 *
 * @package App\Http\Controllers\API
 */

Route::prefix('assistant')->group(function () {
    /**
     * Authentication Routes
     *
     * These routes handle authentication for assistants.
     *
     * @group Authentication
     *
     * @post /auth/register Register a new assistant.
     * @post /auth/login Log in an assistant.
     * @post /auth/logout Log out an assistant.
     * @get /auth/me Get authenticated assistant's information.
     */
    Route::prefix('auth')->group(function () {
        Route::post('/register', [UserAuthController::class, 'register']);
        Route::post('/login', [UserAuthController::class, 'login']);
        Route::post('/logout', [UserAuthController::class, 'logout'])->middleware(['auth:sanctum', 'ability:assistant']);
        Route::get('/me', [UserAuthController::class, 'info'])->middleware(['auth:sanctum', 'ability:assistant']);
    });

    /**
     * Partner Routes
     *
     * These routes handle partner management for assistants.
     *
     * @group Partner Management
     *
     * @get /partner Get all partners.
     * @post /partner Create a new partner.
     * @patch /partner/{id} Update an existing partner.
     * @delete /partner/{id} Delete a partner.
     */
    Route::prefix('partner')->middleware(['auth:sanctum', 'ability:assistant'])->group(function () {
        Route::get('/', [PartnerController::class, 'index']);
        Route::post('/', [PartnerController::class, 'store']);
        Route::patch('/{id}', [PartnerController::class, 'update']);
        Route::delete('/{id}', [PartnerController::class, 'destroy']);
    });

    /**
     * Chat Routes
     *
     * These routes handle chat functionalities for assistants.
     *
     * @group Chat
     *
     * @get /chat Get all chats.
     * @post /chat Create a new chat.
     * @delete /chat/{id} Delete a chat.
     */
    Route::prefix('chat')->middleware(['auth:sanctum', 'ability:assistant'])->group(function () {
        Route::get('/', [ChatController::class, 'index']);
        Route::post('/', [ChatController::class, 'store']);
        Route::delete('/{id}', [ChatController::class, 'destroy']);
    });

    /**
     * Revenue Summary Routes
     *
     * These routes handle revenue summary functionalities for assistants.
     *
     * @group Revenue Summary
     *
     * @get /revenue-summaries Get all revenue summaries.
     * @post /revenue-summaries Create a new revenue summary.
     * @patch /revenue-summaries/{id} Update an existing revenue summary.
     * @delete /revenue-summaries/{id} Delete a revenue summary.
     */
    Route::prefix('revenue-summaries')->middleware(['auth:sanctum', 'ability:assistant'])->group(function () {
        Route::get('/', [RevenueSummaryController::class, 'index']);
        Route::post('/', [RevenueSummaryController::class, 'store']);
        Route::patch('/{id}', [RevenueSummaryController::class, 'update']);
        Route::delete('/{id}', [RevenueSummaryController::class, 'destroy']);
    });
});

Route::prefix('partner')->group(function () {
    /**
     * Partner Authentication Routes
     *
     * These routes handle authentication for partners.
     *
     * @group Authentication
     *
     * @post /auth/register Register a new partner.
     * @post /auth/login Log in a partner.
     * @post /auth/logout Log out a partner.
     */
    Route::prefix('auth')->group(function () {
        Route::post('/register', [PartnerAuthController::class, 'register']);
        Route::post('/login', [PartnerAuthController::class, 'login']);
        Route::post('/logout', [PartnerAuthController::class, 'logout'])->middleware(['auth:sanctum', 'ability:partner']);
    });

    /**
     * Partner Chat Routes
     *
     * These routes handle chat functionalities for partners.
     *
     * @group Chat
     *
     * @get /chat Get all partner chats.
     */
    Route::prefix('chat')->middleware(['auth:sanctum', 'ability:partner'])->group(function () {
        Route::get('/', [PartnerChatController::class, 'index']);
    });
});





