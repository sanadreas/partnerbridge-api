<?php

use App\Http\Controllers\ChatController;
use Illuminate\Support\Facades\Route;

/**
 * Web Routes
 *
 * These routes handle client-side functionalities for the application.
 *
 * @group Client-side
 *
 * @prefix /
 *
 * @package App\Http\Controllers\Web
 */

Route::prefix('chat')->middleware('auth.hashed')->group(function () {
    /**
     * Chat Routes
     *
     * These routes handle chat functionalities for the client-side.
     *
     * @group Chat
     *
     * @get /chat Show chat page.
     * @get /chat/messages Get chat messages.
     * @post /chat/send Send a message in the chat.
     */
    Route::get('/', [ChatController::class, 'show']);
    Route::get('/messages', [ChatController::class, 'messages']);
    Route::post('/send', [ChatController::class, 'send']);
});


