<?php

namespace Tests\Feature;

use App\Models\Chat;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ChatTest extends TestCase
{
    use DatabaseTransactions, WithFaker;

    protected function getAuthToken(): string
    {
        $user = User::factory()->create();

        return $user->createToken('test-token')->plainTextToken;
    }
    /**
     * Test getting all chats for the authenticated assistant.
     *
     * @return void
     */
    public function testGetAllChats()
    {
        $assistant = User::factory()->create();
        $token = $assistant->createToken('test-token')->plainTextToken;
        Chat::factory()->count(3)->create(['user_id' => $assistant->id]);

        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $token,
        ])->get('/api/assistant/chat');

        $response->assertStatus(200);
        $response->assertJsonStructure([
            '*' => [
                'id',
                'user_id',
                'partner_id',
                'hash_url',
                'created_at',
                'updated_at',
            ],
        ]);
    }

    /**
     * Test storing a new chat.
     *
     * @return void
     */
    public function testStoreNewChat()
    {
        $token = $this->getAuthToken();

        $partnerId = $this->faker->numberBetween(1, 3);

        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $token,
        ])->post('/api/assistant/chat', ['partner_id' => $partnerId]);

        $response->assertStatus(201);
        $response->assertJsonStructure([
            'chat_id',
            'chat_page_url' => [
                'assistant',
                'partner',
            ],
        ]);
    }

    /**
     * Test deleting a chat.
     *
     * @return void
     */
    public function testDeleteChat()
    {
        $chat = Chat::factory()->create();
        $token = $this->getAuthToken();
        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $token,
        ])->delete("/api/assistant/chat/{$chat->id}");

        $response->assertStatus(200);
        $response->assertJson([
            'message' => 'Chat deleted',
        ]);
    }
}
