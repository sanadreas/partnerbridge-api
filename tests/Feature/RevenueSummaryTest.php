<?php

namespace Tests\Feature;

use App\Models\RevenueSummary;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class RevenueSummaryTest extends TestCase
{
    use RefreshDatabase;

    protected function getAuthToken(): array
    {
        $user = User::factory()->create();
        $token = $user->createToken('test-token')->plainTextToken;
        return [$user, $token];
    }

    /**
     * Test retrieving revenue summaries for the authenticated user.
     *
     * @return void
     */
    public function testRetrieveRevenueSummaries()
    {
        [$user, $token] = $this->getAuthToken();

        RevenueSummary::factory()->count(3)->create(['user_id' => $user->id]);

        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $token,
        ])->get('/api/assistant/revenue-summaries');

        $response->assertStatus(200)
            ->assertJsonStructure(['data' => []]);
    }

    /**
     * Test storing a new revenue summary.
     *
     * @return void
     */
    public function testStoreRevenueSummary()
    {
        [$user, $token] = $this->getAuthToken();

        $data = [
            'year' => 2023,
            'month' => 3,
            'revenue' => 1000.50,
        ];

        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $token,
        ])->post('/api/assistant/revenue-summaries', $data);

        $response->assertStatus(201)
            ->assertJson([
                'message' => 'Summary created successfully',
                'data' => $data
            ]);
    }

    /**
     * Test updating an existing revenue summary.
     *
     * @return void
     */
    public function testUpdateRevenueSummary()
    {
        [$user, $token] = $this->getAuthToken();

        $summary = RevenueSummary::factory()->create(['user_id' => $user->id]);

        $data = [
            'year' => 2023,
            'month' => 3,
            'revenue' => 1010.50,
        ];

        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $token,
        ])->patch("/api/assistant/revenue-summaries/{$summary->id}", $data);

        $response->assertStatus(200)
            ->assertJson([
                'message' => 'Summary updated successfully',
                'data' => $data
            ]);
    }

    /**
     * Test deleting a revenue summary.
     *
     * @return void
     */
    public function testDeleteRevenueSummary()
    {
        [$user, $token] = $this->getAuthToken();

        $summary = RevenueSummary::factory()->create(['user_id' => $user->id]);

        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $token,
        ])->delete("/api/assistant/revenue-summaries/{$summary->id}");

        $response->assertStatus(200)
            ->assertJson(['message' => 'Summary deleted successfully']);
    }
}
