<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class AssistantAuthTest extends TestCase
{
    use DatabaseTransactions;
    use WithFaker;

    /**
     * Test Assistant Registration.
     *
     * @return void
     */
    public function testRegistration()
    {
        $userData = [
            'name' => $this->faker->name,
            'email' => $this->faker->unique()->safeEmail,
            'password' => 'password123',
        ];

        $response = $this->json('POST', '/api/assistant/auth/register', $userData);

        $response->assertStatus(200)
            ->assertJson([
                'message' => 'User Created',
            ]);
    }

    /**
     * Test Assistant Login.
     *
     * @return void
     */
    public function testAssistantLogin()
    {
        $user = User::factory()->create();

        $userData = [
            'email' => $user->email,
            'password' => 'password',
        ];

        $response = $this->json('POST', '/api/assistant/auth/login', $userData);

        $response->assertStatus(200)
            ->assertJsonStructure([
                'access_token',
                'user' => [
                    'id',
                    'name',
                    'email',
                ],
            ]);
    }

    /**
     * Test Assistant Logout.
     *
     * @return void
     */
    public function testAssistantLogout()
    {
        $user = User::factory()->create();

        $token = $user->createToken($user->name . '-AssistantToken', ['assistant'])->plainTextToken;

        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $token,
        ])->json('POST', '/api/assistant/auth/logout');

        $response->assertStatus(200)
            ->assertJson([
                'message' => 'logged out',
            ]);
    }
}
