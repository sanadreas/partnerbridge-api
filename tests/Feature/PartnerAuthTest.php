<?php

namespace Tests\Feature;

use App\Models\Partner;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class PartnerAuthTest extends TestCase
{
    use DatabaseTransactions;
    use WithFaker;

    /**
     * Test Partner Registration.
     *
     * @return void
     */
    public function testAssistantRegistration()
    {
        $userData = [
            'name' => $this->faker->name,
            'email' => $this->faker->unique()->safeEmail,
            'password' => 'password123',
        ];

        $response = $this->json('POST', '/api/partner/auth/register', $userData);

        $response->assertStatus(200)
            ->assertJson([
                'message' => 'Partner Created',
            ]);
    }

    /**
     * Test Partner Login.
     *
     * @return void
     */
    public function testLogin()
    {
        $user = Partner::factory()->create();

        $userData = [
            'email' => $user->email,
            'password' => 'password12',
        ];

        $response = $this->json('POST', '/api/partner/auth/login', $userData);

        $response->assertStatus(200)
            ->assertJsonStructure([
                'access_token',
                'partner' => [
                    'id',
                    'name',
                    'email',
                    'created_at',
                    'updated_at'
                ],
            ]);
    }

    /**
     * Test Partner Logout.
     *
     * @return void
     */
    public function testLogout()
    {
        $user = Partner::factory()->create();

        $token = $user->createToken($user->name . '-AssistantToken', ['partner'])->plainTextToken;

        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $token,
        ])->json('POST', '/api/partner/auth/logout');

        $response->assertStatus(200)
            ->assertJson([
                'message' => 'logged out',
            ]);
    }
}
