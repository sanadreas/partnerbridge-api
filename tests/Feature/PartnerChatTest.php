<?php

namespace Tests\Feature;

use App\Models\Chat;
use App\Models\Partner;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class PartnerChatTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    /**
     * Test retrieving all partner chats.
     *
     * @return void
     */
    public function testRetrieveAllPartnerChats()
    {
        $partner = Partner::factory()->create();
        $token = $partner->createToken($partner->name . '-AssistantToken', ['partner'])->plainTextToken;

        $chats = Chat::factory()->count(3)->create(['partner_id' => $partner->id]);

        $expectedChatUrls = [];
        foreach ($chats as $chat) {
            $partnerUrl = url('/chat') . '?token=' . $chat->hash_url . '&user=p' . $chat->partner_id;
            $expectedChatUrls[] = [
                'chat_id' => $chat->id,
                'assistant' => [
                    'id' => $chat->user->id,
                    'name' => $chat->user->name,
                    'email' => $chat->user->email,
                    'created_at' => $chat->user->created_at->toISOString(),
                    'updated_at' => $chat->user->updated_at->toISOString(),
                ],
                'chat_url' => $partnerUrl,
            ];
        }

        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $token,
        ])->get('/api/partner/chat');

        $response->assertStatus(200)
            ->assertJson(['chats' => $expectedChatUrls]);
    }
}
