<?php

namespace Tests\Feature;

use App\Models\Partner;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class PartnerCRUDTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Получить токен аутентификации для теста.
     *
     * @return string
     */
    protected function getAuthToken(): string
    {
        $user = User::factory()->create();

        return $user->createToken('test-token')->plainTextToken;
    }

    /**
     * Тест для получения всех партнеров.
     *
     * @return void
     */
    public function testGetAllPartners()
    {
        $token = $this->getAuthToken();

        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $token,
        ])->get('/api/assistant/partner');

        $response->assertStatus(200)
            ->assertJsonStructure([
                '*' => [
                    'id',
                    'name',
                    'email',
                    'created_at',
                    'updated_at',
                ],
            ]);
    }

    /**
     * Тест для создания нового партнера.
     *
     * @return void
     */
    public function testCreateNewPartner()
    {
        $partnerData = [
            'name' => 'Test Partner ' . uniqid(),
            'email' => 'test_' . uniqid() . '@example.com',
            'password' => 'password123',
        ];

        $token = $this->getAuthToken();

        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $token,
        ])->post('/api/assistant/partner', $partnerData);

        $response->assertStatus(201)
            ->assertJsonStructure([
                'partner' => [
                    'name',
                    'email',
                    'created_at',
                    'updated_at',
                    'id',
                ],
            ]);
    }

    /**
     * Тест для обновления существующего партнера.
     *
     * @return void
     */
    public function testUpdatePartner()
    {
        $partner = Partner::factory()->create();

        $data = [
            'name' => 'Updated Partner Name',
        ];
        $token = $this->getAuthToken();
        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $token,
        ])->patch("/api/assistant/partner/{$partner->id}", $data);

        $response->assertStatus(200)
            ->assertJson([
                'partner' => [
                    'id' => $partner->id,
                    'name' => 'Updated Partner Name',
                ],
            ]);
    }

    /**
     * Тест для удаления партнера.
     *
     * @return void
     */
    public function testDeletePartner()
    {
        $partner = Partner::factory()->create();
        $token = $this->getAuthToken();
        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $token,
        ])->delete("/api/assistant/partner/{$partner->id}");

        $response->assertStatus(200)
            ->assertJson([
                'message' => 'Partner deleted',
            ]);
    }
}
