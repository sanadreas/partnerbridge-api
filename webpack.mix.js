const mix = require('laravel-mix');

mix.webpackConfig({
    resolve: {
        fallback: {
            path: require.resolve("path-browserify"),
            os: require.resolve("os-browserify/browser"),
            crypto: require.resolve("crypto-browserify"),
            vm: require.resolve("vm-browserify"),
            stream: require.resolve("stream-browserify")
        }
    }
});

mix.js('resources/js/app.js', 'public/js')
    .vue()
    .postCss('resources/css/app.css', 'public/css', [
        require('tailwindcss')
    ]);
