<?php

namespace App\Http\Controllers\Auth;

use App\Exceptions\HandlesModelNotFoundException;
use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Auth\RegistrationRequest;
use App\Models\User;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Hash;

/**
 * @OA\Tag(
 *     name="Authentication Assistant",
 *     description="Controller responsible for assistant authentication."
 * )
 */
class UserAuthController extends Controller
{
    use HandlesModelNotFoundException;

    /**
     * Register a new assistant.
     *
     * @OA\Post(
     *     path="/api/assistant/auth/register",
     *     summary="Register a new assistant",
     *     tags={"Authentication Assistant"},
     *     @OA\RequestBody(
     *         required=true,
     *         description="User registration data",
     *         @OA\JsonContent(
     *             required={"name", "email", "password"},
     *             @OA\Property(property="name", type="string"),
     *             @OA\Property(property="email", type="string"),
     *             @OA\Property(property="password", type="string")
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="User Created",
     *         @OA\JsonContent(
     *             @OA\Property(property="message", type="string", example="User Created")
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Registration failed",
     *         @OA\JsonContent(
     *             @OA\Property(property="error", type="string", example="Registration failed: Error message")
     *         )
     *     )
     * )
     *
     * @param RegistrationRequest $request
     * @return JsonResponse
     */
    public function register(RegistrationRequest $request): JsonResponse
    {
        try {
            $registerUserData = $request->validated();
            $registerUserData['password'] = Hash::make($registerUserData['password']);

            User::create($registerUserData);

            return response()->json([
                'message' => 'User Created',
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'error' => 'Registration failed: ' . $e->getMessage()
            ], 500);
        }
    }

    /**
     * Log in a assistant.
     *
     * @OA\Post(
     *     path="/api/assistant/auth/login",
     *     summary="Log in a assistant",
     *     tags={"Authentication Assistant"},
     *     @OA\RequestBody(
     *         required=true,
     *         description="User login credentials",
     *         @OA\JsonContent(
     *             required={"email", "password"},
     *             @OA\Property(property="email", type="string"),
     *             @OA\Property(property="password", type="string")
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Login successful",
     *         @OA\JsonContent(
     *             @OA\Property(property="access_token", type="string"),
     *             @OA\Property(
     *                 property="user",
     *                 type="object",
     *                 @OA\Property(property="id", type="integer"),
     *                 @OA\Property(property="name", type="string"),
     *                 @OA\Property(property="email", type="string")
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Invalid credentials",
     *         @OA\JsonContent(
     *             @OA\Property(property="message", type="string", example="Invalid Credentials")
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Login failed",
     *         @OA\JsonContent(
     *             @OA\Property(property="error", type="string", example="Login failed: Error message")
     *         )
     *     )
     * )
     *
     * @param LoginRequest $request
     * @return JsonResponse
     */
    public function login(LoginRequest $request): JsonResponse
    {
        $loginUserData = $request->validated();

        try {
            $user = User::where('email', $loginUserData['email'])->firstOrFail();
        } catch (ModelNotFoundException) {
            return $this->handleModelNotFoundException('User');
        }
        try {
            if (!$user || !Hash::check($loginUserData['password'], $user->password)) {
                if (!Hash::check($loginUserData['password'], $user->password)) {
                    throw new AuthenticationException('Invalid Credentials');
                }
            }

            $token = $user->createToken($user->name . '-AssistantToken', ['assistant'], now()->addRealHours(12))
                ->plainTextToken;

            return response()->json([
                'access_token' => $token,
                'user' => $user
            ]);
        } catch (AuthenticationException $e) {
            return response()->json([
                'message' => $e->getMessage()
            ], 401);
        } catch (\Exception $e) {
            return response()->json([
                'error' => 'Login failed: ' . $e->getMessage()
            ], 500);
        }
    }

    /**
     * Log out a assistant.
     *
     * @OA\Post(
     *     path="/api/assistant/auth/logout",
     *     summary="Log out a assistant",
     *     tags={"Authentication Assistant"},
     *     security={{"sanctum": {}}},
     *     @OA\Response(
     *         response=200,
     *         description="Logged out",
     *         @OA\JsonContent(
     *             @OA\Property(property="message", type="string", example="logged out")
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Logout failed",
     *         @OA\JsonContent(
     *             @OA\Property(property="error", type="string", example="Logout failed: Error message")
     *         )
     *     )
     * )
     *
     * @return JsonResponse
     */
    public function logout(): JsonResponse
    {
        try {
            auth()->user()->tokens()->delete();

            return response()->json([
                "message" => "logged out"
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'error' => 'Logout failed: ' . $e->getMessage()
            ], 500);
        }
    }

    /**
     * Get authenticated assistant's information.
     *
     * @response {
     *    "id": 1,
     *    "name": "John Doe",
     *    "email": "john@example.com"
     * }
     *
     * @return Authenticatable
     */
    public function info(): Authenticatable
    {
        return auth()->user();
    }
}
