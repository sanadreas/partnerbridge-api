<?php

namespace App\Http\Controllers\Auth;

use App\Exceptions\HandlesModelNotFoundException;
use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Auth\RegistrationRequest;
use App\Models\Partner;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Hash;

/**
 * @OA\Tag(
 *     name="Authentication Partner",
 *     description="Controller responsible for partner authentication."
 * )
 */
class PartnerAuthController extends Controller
{
    use HandlesModelNotFoundException;

    /**
     * Register a new partner.
     *
     * @OA\Post(
     *     path="/api/partner/auth/register",
     *     summary="Register a new partner",
     *     tags={"Authentication Partner"},
     *     @OA\RequestBody(
     *         required=true,
     *         description="Partner registration data",
     *         @OA\JsonContent(
     *             required={"name", "email", "password"},
     *             @OA\Property(property="name", type="string"),
     *             @OA\Property(property="email", type="string"),
     *             @OA\Property(property="password", type="string")
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Partner Created",
     *         @OA\JsonContent(
     *             @OA\Property(property="message", type="string", example="Partner Created")
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Registration failed",
     *         @OA\JsonContent(
     *             @OA\Property(property="error", type="string", example="Registration failed: Error message")
     *         )
     *     )
     * )
     *
     * @param RegistrationRequest $request
     * @return JsonResponse
     */
    public function register(RegistrationRequest $request): JsonResponse
    {
        try {
            $registerPartnerData = $request->validated();
            $registerPartnerData['password'] = Hash::make($registerPartnerData['password']);

            Partner::create($registerPartnerData);

            return response()->json([
                'message' => 'Partner Created',
            ]);
        } catch (\Exception) {
            return response()->json(['error' => 'An error occurred while registering partner.'], 500);
        }
    }

    /**
     * Log in a partner.
     *
     * @OA\Post(
     *     path="/api/partner/auth/login",
     *     summary="Log in a partner",
     *     tags={"Authentication Partner"},
     *     @OA\RequestBody(
     *         required=true,
     *         description="Partner login credentials",
     *         @OA\JsonContent(
     *             required={"email", "password"},
     *             @OA\Property(property="email", type="string"),
     *             @OA\Property(property="password", type="string")
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Login successful",
     *         @OA\JsonContent(
     *             @OA\Property(property="access_token", type="string"),
     *             @OA\Property(
     *                 property="partner",
     *                 type="object",
     *                 @OA\Property(property="id", type="integer"),
     *                 @OA\Property(property="name", type="string"),
     *                 @OA\Property(property="email", type="string")
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Invalid credentials",
     *         @OA\JsonContent(
     *             @OA\Property(property="message", type="string", example="Invalid Credentials")
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Login failed",
     *         @OA\JsonContent(
     *             @OA\Property(property="error", type="string", example="Login failed: Error message")
     *         )
     *     )
     * )
     *
     * @param LoginRequest $request
     * @return JsonResponse
     */
    public function login(LoginRequest $request): JsonResponse
    {
        try {
            $loginPartnerData = $request->validated();

            $partner = Partner::where('email', $loginPartnerData['email'])->first();

            if (!$partner || !Hash::check($loginPartnerData['password'], $partner->password)) {
                return response()->json([
                    'message' => 'Invalid Credentials'
                ], 401);
            }

            $token = $partner->createToken($partner->name . '-PartnerToken', ['partner'], now()->addRealHours(12))->plainTextToken;

            return response()->json([
                'access_token' => $token,
                'partner' => $partner
            ]);
        } catch (ModelNotFoundException) {
            return $this->handleModelNotFoundException('Partner');
        } catch (\Exception) {
            return response()->json(['error' => 'An error occurred while logging in partner.'], 500);
        }
    }

    /**
     * Log out a partner.
     *
     * @OA\Post(
     *     path="/api/partner/auth/logout",
     *     summary="Log out a partner",
     *     tags={"Authentication Partner"},
     *     security={{"sanctum": {}}},
     *     @OA\Response(
     *         response=200,
     *         description="Logged out",
     *         @OA\JsonContent(
     *             @OA\Property(property="message", type="string", example="logged out")
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Logout failed",
     *         @OA\JsonContent(
     *             @OA\Property(property="error", type="string", example="Logout failed: Error message")
     *         )
     *     )
     * )
     *
     * @return JsonResponse
     */
    public function logout(): JsonResponse
    {
        try {
            auth()->user()->tokens()->delete();

            return response()->json([
                "message" => "logged out"
            ]);
        } catch (\Exception) {
            return response()->json(['error' => 'An error occurred while logging out.'], 500);
        }
    }
}
