<?php

namespace App\Http\Controllers;

use App\Exceptions\HandlesModelNotFoundException;
use App\Http\Requests\RevenueSummaryRequest;
use App\Models\RevenueSummary;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;

/**
 * @OA\Tag(
 *     name="Revenue Summary",
 *     description="Controller handle revenue summary functionalities for assistants."
 * )
 */
class RevenueSummaryController extends Controller
{
    use HandlesModelNotFoundException;

    /**
     * Retrieve revenue summaries for the authenticated user.
     *
     * @OA\Get(
     *     path="/api/assistant/revenue-summaries",
     *     summary="Retrieve revenue summaries",
     *     tags={"Revenue Summary"},
     *     security={{ "bearerAuth": {} }},
     *     @OA\Response(
     *         response=200,
     *         description="Revenue summaries retrieved successfully",
     *         @OA\JsonContent(
     *             type="object",
     *             @OA\Property(property="data", type="array",
     *                 @OA\Items(
     *                     type="object",
     *                     @OA\Property(property="id", type="integer", description="Summary ID"),
     *                     @OA\Property(property="user_id", type="integer", description="User ID"),
     *                 )
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Internal server error",
     *         @OA\JsonContent(
     *             @OA\Property(property="error", type="string"),
     *         )
     *     )
     * )
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        try {
            $summaries = RevenueSummary::where('user_id', auth()->id())->get();
            return response()->json(['data' => $summaries]);
        } catch (\Exception) {
            return response()->json(['error' => 'An error occurred while fetching summaries.'], 500);
        }
    }

    /**
     * Store a new revenue summary.
     *
     * @OA\Post(
     *     path="/api/assistant/revenue-summaries",
     *     summary="Store a new revenue summary",
     *     tags={"Revenue Summary"},
     *     security={{ "bearerAuth": {} }},
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="object",
     *                 required={"year", "month", "revenue"},
     *                 @OA\Property(property="year", type="integer", description="The year of the revenue summary"),
     *                 @OA\Property(property="month", type="integer", description="The month of the revenue summary"),
     *                 @OA\Property(property="revenue", type="number", description="The revenue amount of the summary"),
     *                 @OA\Property(property="category", type="string", nullable=true, description="The category of the revenue summary"),
     *                 @OA\Property(property="product", type="string", nullable=true, description="The product related to the revenue summary"),
     *                 @OA\Property(property="sales_count", type="integer", nullable=true, description="The count of sales related to the revenue summary"),
     *                 @OA\Property(property="average_receipt", type="number", nullable=true, description="The average receipt amount related to the revenue summary"),
     *                 @OA\Property(property="revenue_before_tax", type="number", nullable=true, description="The revenue amount before tax related to the revenue summary"),
     *                 @OA\Property(property="tax_amount", type="number", nullable=true, description="The tax amount related to the revenue summary"),
     *                 @OA\Property(property="revenue_after_tax", type="number", nullable=true, description="The revenue amount after tax related to the revenue summary"),
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=201,
     *         description="Summary created successfully",
     *         @OA\JsonContent(
     *             type="object",
     *             @OA\Property(property="message", type="string", description="Success message"),
     *             @OA\Property(property="data", type="object",
     *                 @OA\Property(property="id", type="integer", description="Summary ID"),
     *                 @OA\Property(property="user_id", type="integer", description="User ID"),
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Internal server error",
     *         @OA\JsonContent(
     *             @OA\Property(property="error", type="string"),
     *         )
     *     )
     * )
     *
     * @param RevenueSummaryRequest $request
     * @return JsonResponse
     */
    public function store(RevenueSummaryRequest $request): JsonResponse
    {
        try {
            $summary = new RevenueSummary();
            $summary->fill($request->validated());
            $summary->user_id = auth()->id();
            $summary->save();

            return response()->json(['message' => 'Summary created successfully', 'data' => $summary], 201);
        } catch (\Exception) {
            return response()->json(['error' => 'An error occurred while creating summary.'], 500);
        }
    }

    /**
     * Update an existing revenue summary.
     *
     * @OA\Patch(
     *     path="/api/assistant/revenue-summaries/{id}",
     *     summary="Update an existing revenue summary",
     *     tags={"Revenue Summary"},
     *     security={{ "bearerAuth": {} }},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="ID of the revenue summary to update",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\RequestBody(
     *          required=true,
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                  type="object",
     *                  required={"year", "month", "revenue"},
     *                  @OA\Property(property="year", type="integer", description="The year of the revenue summary"),
     *                  @OA\Property(property="month", type="integer", description="The month of the revenue summary"),
     *                  @OA\Property(property="revenue", type="number", description="The revenue amount of the summary"),
     *                  @OA\Property(property="category", type="string", nullable=true, description="The category of the revenue summary"),
     *                  @OA\Property(property="product", type="string", nullable=true, description="The product related to the revenue summary"),
     *                  @OA\Property(property="sales_count", type="integer", nullable=true, description="The count of sales related to the revenue summary"),
     *                  @OA\Property(property="average_receipt", type="number", nullable=true, description="The average receipt amount related to the revenue summary"),
     *                  @OA\Property(property="revenue_before_tax", type="number", nullable=true, description="The revenue amount before tax related to the revenue summary"),
     *                  @OA\Property(property="tax_amount", type="number", nullable=true, description="The tax amount related to the revenue summary"),
     *                  @OA\Property(property="revenue_after_tax", type="number", nullable=true, description="The revenue amount after tax related to the revenue summary"),
     *              )
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="Summary updated successfully",
     *         @OA\JsonContent(
     *             type="object",
     *             @OA\Property(property="message", type="string", description="Success message"),
     *             @OA\Property(property="data", type="object",
     *                 @OA\Property(property="id", type="integer", description="Summary ID"),
     *                 @OA\Property(property="user_id", type="integer", description="User ID"),
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Summary not found",
     *         @OA\JsonContent(
     *             @OA\Property(property="error", type="string"),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Internal server error",
     *         @OA\JsonContent(
     *             @OA\Property(property="error", type="string"),
     *         )
     *     )
     * )
     *
     * @param RevenueSummaryRequest $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(RevenueSummaryRequest $request, $id): JsonResponse
    {
        try {
            $summary = RevenueSummary::where('user_id', auth()->id())->findOrFail($id);
            $summary->fill($request->validated());
            $summary->save();

            return response()->json(['message' => 'Summary updated successfully', 'data' => $summary]);
        } catch (ModelNotFoundException) {
            return $this->handleModelNotFoundException('Summary');
        } catch (\Exception) {
            return response()->json(['error' => 'An error occurred while updating summary.'], 500);
        }
    }

    /**
     * Delete a revenue summary.
     *
     * @OA\Delete(
     *     path="/api/assistant/revenue-summaries/{id}",
     *     summary="Delete a revenue summary",
     *     tags={"Revenue Summary"},
     *     security={{ "bearerAuth": {} }},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="ID of the revenue summary to delete",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Summary deleted successfully",
     *         @OA\JsonContent(
     *             @OA\Property(property="message", type="string")
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Summary not found",
     *         @OA\JsonContent(
     *             @OA\Property(property="error", type="string"),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Internal server error",
     *         @OA\JsonContent(
     *             @OA\Property(property="error", type="string"),
     *         )
     *     )
     * )
     *
     * @param int $id
     * @return JsonResponse
     */
    public function destroy($id): JsonResponse
    {
        try {
            $summary = RevenueSummary::where('user_id', auth()->id())->findOrFail($id);
            $summary->delete();

            return response()->json(['message' => 'Summary deleted successfully']);
        } catch (ModelNotFoundException) {
            return $this->handleModelNotFoundException('Summary');
        } catch (\Exception) {
            return response()->json(['error' => 'An error occurred while deleting summary.'], 500);
        }
    }
}
