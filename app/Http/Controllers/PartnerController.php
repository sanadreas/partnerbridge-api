<?php

namespace App\Http\Controllers;

use App\Exceptions\HandlesModelNotFoundException;
use App\Http\Requests\Auth\RegistrationRequest;
use App\Models\Partner;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Eloquent\Collection;

/**
 * @OA\Tag(
 *     name="Partner",
 *     description="Controller responsible for managing partners."
 * )
 */
class PartnerController extends Controller
{
    use HandlesModelNotFoundException;

    /**
     * @OA\Get(
     *     path="/api/assistant/partner",
     *     summary="Get all partners",
     *     tags={"Partner"},
     *     @OA\Response(
     *         response=200,
     *         description="Successful operation",
     *         @OA\JsonContent(
     *             type="array",
     *             @OA\Items(
     *                 type="object",
     *                 @OA\Property(property="id", type="integer"),
     *                 @OA\Property(property="name", type="string"),
     *                 @OA\Property(property="email", type="string"),
     *                 @OA\Property(property="created_at", type="string", format="date-time"),
     *                 @OA\Property(property="updated_at", type="string", format="date-time"),
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Server Error",
     *         @OA\JsonContent(
     *             @OA\Property(property="error", type="string")
     *         )
     *     )
     * )
     */
    public function index(): Collection
    {
        return Partner::all();
    }

    /**
     * Store a new partner.
     *
     * @OA\Post(
     *     path="/api/assistant/partner",
     *     summary="Store a new partner",
     *     tags={"Partner"},
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(
     *             type="object",
     *             @OA\Property(property="name", type="string"),
     *             @OA\Property(property="email", type="string"),
     *             @OA\Property(property="password", type="string", format="password")
     *         )
     *     ),
     *     @OA\Response(
     *         response=201,
     *         description="Partner created successfully",
     *         @OA\JsonContent(
     *             type="object",
     *                   @OA\Property(property="id", type="integer"),
     *                   @OA\Property(property="name", type="string"),
     *                   @OA\Property(property="email", type="string"),
     *                   @OA\Property(property="created_at", type="string", format="date-time"),
     *                   @OA\Property(property="updated_at", type="string", format="date-time"),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Internal server error"
     *     )
     * )
     *
     * @param RegistrationRequest $request
     * @return JsonResponse
     */
    public function store(RegistrationRequest $request): JsonResponse
    {
        try {
            $validatedData = $request->validated();

            $validatedData['password'] = Hash::make($validatedData['password']);

            $partner = Partner::create($validatedData);

            return response()->json(['partner' => $partner], 201);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    /**
     * Update an existing partner.
     *
     * @OA\Patch(
     *     path="/api/assistant/partner/{id}",
     *     summary="Update an existing partner",
     *     tags={"Partner"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="ID of the partner to update",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(
     *             type="object",
     *             @OA\Property(property="name", type="string"),
     *             @OA\Property(property="email", type="string"),
     *             @OA\Property(property="password", type="string", format="password")
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Partner updated successfully",
     *         @OA\JsonContent(
     *             type="object",
     *                   @OA\Property(property="id", type="integer"),
     *                   @OA\Property(property="name", type="string"),
     *                   @OA\Property(property="email", type="string"),
     *                   @OA\Property(property="created_at", type="string", format="date-time"),
     *                   @OA\Property(property="updated_at", type="string", format="date-time"),
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Partner not found"
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Internal server error"
     *     )
     * )
     *
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(Request $request, int $id): JsonResponse
    {
        try {
            $partner = Partner::findOrFail($id);

            $validatedData = $request->validate([
                'name' => 'string',
                'email' => 'email|unique:partners,email',
                'password' => 'nullable|string|min:8',
            ]);

            if (isset($validatedData['password'])) {
                $validatedData['password'] = Hash::make($validatedData['password']);
            }

            $partner->update($validatedData);

            return response()->json(['partner' => $partner]);
        } catch (ModelNotFoundException) {
            return $this->handleModelNotFoundException('Partner');
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    /**
     * Delete a partner.
     *
     * @OA\Delete(
     *     path="/api/assistant/partner/{id}",
     *     summary="Delete a partner",
     *     tags={"Partner"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="ID of the partner to delete",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Partner deleted successfully"
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Partner not found"
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Internal server error"
     *     )
     * )
     *
     * @param int $id
     * @return JsonResponse
     */
    public function destroy(int $id): JsonResponse
    {
        try {
            $partner = Partner::findOrFail($id);

            $partner->delete();

            return response()->json(['message' => 'Partner deleted']);
        } catch (ModelNotFoundException) {
            return $this->handleModelNotFoundException('Partner');
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }
}
