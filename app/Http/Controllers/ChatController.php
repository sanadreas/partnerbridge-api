<?php

namespace App\Http\Controllers;

use App\Events\MessageSent;
use App\Exceptions\HandlesModelNotFoundException;
use App\Http\Requests\MessageFormRequest;
use App\Http\Requests\PartnerRequest;
use App\Models\Chat;
use App\Models\Message;
use App\Models\User;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Application;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Str;

/**
 * @OA\Tag(
 *     name="Chat",
 *     description="Endpoints for managing chats"
 * )
 */
class ChatController extends Controller
{
    use HandlesModelNotFoundException;

    /**
     * Get all chats for the authenticated assistant.
     *
     * @OA\Get(
     *     path="/api/assistant/chat",
     *     summary="Get all chats",
     *     tags={"Chat"},
     *     security={{ "bearerAuth": {} }},
     *     @OA\Response(
     *         response=200,
     *         description="Successful operation",
     *         @OA\JsonContent(
     *              type="array",
     *              @OA\Items(
     *                  type="object",
     *                  @OA\Property(property="id", type="integer"),
     *                  @OA\Property(property="user_id", type="integer"),
     *                  @OA\Property(property="partner_id", type="integer"),
     *                  @OA\Property(property="created_at", type="string", format="date-time"),
     *                  @OA\Property(property="updated_at", type="string", format="date-time")
     *              )
     *          )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Internal server error",
     *         @OA\JsonContent(
     *             @OA\Property(property="error", type="string")
     *         )
     *     )
     * )
     *
     * @return array
     */
    public function index(): Collection
    {
        try {
            $assistant = auth()->user();

            return Chat::where('user_id', $assistant->id)->get();
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    /**
     * Store a new chat.
     *
     * @OA\Post(
     *     path="/api/assistant/chat",
     *     summary="Store a new chat",
     *     tags={"Chat"},
     *     security={{ "bearerAuth": {} }},
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(
     *             required={"partner_id"},
     *             @OA\Property(property="partner_id", type="integer", description="ID of the partner"),
     *         )
     *     ),
     *     @OA\Response(
     *         response=201,
     *         description="Chat created successfully",
     *         @OA\JsonContent(
     *             @OA\Property(property="chat_id", type="integer", description="ID of the created chat"),
     *             @OA\Property(property="chat_page_url", type="object",
     *                 @OA\Property(property="assistant", type="string", description="URL for assistant chat page"),
     *                 @OA\Property(property="partner", type="string", description="URL for partner chat page"),
     *             ),
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Bad request",
     *         @OA\JsonContent(
     *             @OA\Property(property="error", type="string", description="Validation errors"),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Internal server error",
     *         @OA\JsonContent(
     *             @OA\Property(property="error", type="string"),
     *         )
     *     )
     * )
     *
     * @param PartnerRequest $request
     * @return JsonResponse
     */
    public function store(PartnerRequest $request): JsonResponse
    {
        try {
            $assistant = $request->user();

            $validatedData = $request->validated();

            $chat = Chat::where('user_id', $assistant->id)
                ->where('partner_id', $validatedData['partner_id'])
                ->first();

            $hash_token_chat = hash('sha256', $assistant->id . $validatedData['partner_id'] . Str::random(10));

            if (!$chat) {
                $chat = new Chat();
                $chat->user_id = $assistant->id;
                $chat->partner_id = $validatedData['partner_id'];
                $chat->hash_url = $hash_token_chat;
                $chat->save();
            }

            $assistantUrl = url('/chat') . '?token=' . $hash_token_chat . '&user=a' . $assistant->id;
            $partnerUrl = url('/chat') . '?token=' . $hash_token_chat . '&user=p' . $validatedData['partner_id'];

            $response = [
                'chat_id' => $chat->id,
                'chat_page_url' => [
                    'assistant' => $assistantUrl,
                    'partner' => $partnerUrl,
                ],
            ];

            return response()->json($response, 201);
        } catch (ModelNotFoundException) {
            return $this->handleModelNotFoundException('Partner');
        } catch (\Exception) {
            return response()->json(['error' => 'An error occurred while creating chat.'], 500);
        }
    }

    /**
     * Delete a chat.
     *
     * @OA\Delete(
     *     path="/api/assistant/chat/{id}",
     *     summary="Delete a chat",
     *     tags={"Chat"},
     *     security={{ "bearerAuth": {} }},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="ID of the chat to delete",
     *         required=true,
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Chat deleted successfully",
     *         @OA\JsonContent(
     *             @OA\Property(property="message", type="string", description="Success message"),
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Chat not found",
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Internal server error",
     *         @OA\JsonContent(
     *             @OA\Property(property="error", type="string"),
     *         )
     *     )
     * )
     *
     * @param int $id
     * @return JsonResponse
     */
    public function destroy(int $id): JsonResponse
    {
        try {
            $chat = Chat::findOrFail($id);
            $chat->delete();
            return response()->json(['message' => 'Chat deleted']);
        } catch (ModelNotFoundException) {
            return $this->handleModelNotFoundException('Chat');
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    /**
     * Show the chat view.
     *
     * @return View|Application|Factory
     */
    public function show(): View|Application|Factory
    {
        return view('chat');
    }

    /**
     * Retrieve messages for the authenticated user.
     *
     * @OA\Get(
     *     path="chat/messages",
     *     summary="Retrieve messages",
     *     tags={"Chat"},
     *     security={{ "bearerAuth": {} }},
     *     @OA\Response(
     *         response=200,
     *         description="Messages retrieved successfully",
     *         @OA\JsonContent(
     *             type="array",
     *             @OA\Items(
     *                 @OA\Property(property="id", type="integer", description="Message ID"),
     *                 @OA\Property(property="text", type="string", description="Message text"),
     *                 @OA\Property(property="created_at", type="string", format="date-time", description="Message creation timestamp"),
     *                 @OA\Property(property="updated_at", type="string", format="date-time", description="Message last update timestamp"),
     *                 @OA\Property(property="user", type="object",
     *                     @OA\Property(property="id", type="integer", description="User ID"),
     *                     @OA\Property(property="name", type="string", description="User name"),
     *                     @OA\Property(property="email", type="string", description="User email"),
     *                 ),
     *             ),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Internal server error",
     *         @OA\JsonContent(
     *             @OA\Property(property="error", type="string"),
     *         )
     *     )
     * )
     *
     * @return Collection|array
     */
    public function messages(): Collection|array
    {
        try {
            $user = auth()->user();
            $relations = ($user instanceof User) ? 'user' : 'partner';
            $userId = $user->id;
            return Message::where($relations . '_id', $userId)->with($relations)->get();
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    /**
     * Send a message.
     *
     * @OA\Post(
     *     path="/chat/messages/send",
     *     summary="Send a message",
     *     tags={"Chat"},
     *     security={{ "bearerAuth": {} }},
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(
     *             required={"token", "text"},
     *             @OA\Property(property="token", type="string", description="Chat token"),
     *             @OA\Property(property="text", type="string", description="Message text"),
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Message sent successfully",
     *         @OA\JsonContent(
     *             type="object",
     *             @OA\Property(property="id", type="integer", description="Message ID"),
     *             @OA\Property(property="text", type="string", description="Message text"),
     *             @OA\Property(property="created_at", type="string", format="date-time", description="Message creation timestamp"),
     *             @OA\Property(property="updated_at", type="string", format="date-time", description="Message last update timestamp"),
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Chat not found",
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Internal server error",
     *         @OA\JsonContent(
     *             @OA\Property(property="error", type="string"),
     *         )
     *     )
     * )
     *
     * @param MessageFormRequest $request
     * @return Message|array
     */
    public function send(MessageFormRequest $request): array|Message
    {
        try {
            $user = auth()->user();
            $token = $request->query('token');
            $messageData = $request->validated();
            $chat = Chat::where('hash_url', $token)->firstOrFail();
            $messageData['chat_id'] = $chat->id;
            $message = $user->messages()->create($messageData);
            broadcast(new MessageSent($user, $message));
            return $message;
        } catch (ModelNotFoundException) {
            return $this->handleModelNotFoundException('Chat');
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }
}
