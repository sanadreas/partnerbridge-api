<?php

namespace App\Http\Controllers;

use App\Exceptions\HandlesModelNotFoundException;
use App\Models\Chat;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;

/**
 * @OA\Tag(
 *     name="Partner Chat",
 *     description="This controller is responsible for handling chat functionalities for partners."
 * )
 */
class PartnerChatController extends Controller
{
    use HandlesModelNotFoundException;

    /**
     * Get all partner chats.
     *
     * @OA\Get(
     *     path="/api/partner/chat",
     *     summary="Get all partner chats",
     *     tags={"Partner Chat"},
     *     security={{ "bearerAuth": {} }},
     *     @OA\Response(
     *         response=200,
     *         description="Successful operation",
     *         @OA\JsonContent(
     *             type="object",
     *             @OA\Property(property="chats", type="array",
     *                 @OA\Items(
     *                     @OA\Property(property="chat_id", type="integer", description="Chat ID"),
     *                     @OA\Property(property="assistant", type="object",
     *                         @OA\Property(property="id", type="integer", description="Assistant ID"),
     *                         @OA\Property(property="name", type="string", description="Assistant name"),
     *                         @OA\Property(property="email", type="string", description="Assistant email"),
     *                         @OA\Property(property="created_at", type="string", format="date-time", description="Assistant created at"),
     *                         @OA\Property(property="updated_at", type="string", format="date-time", description="Assistant updated at"),
     *                     ),
     *                     @OA\Property(property="chat_url", type="string", description="Chat URL"),
     *                 )
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Partner not found",
     *         @OA\JsonContent(
     *             @OA\Property(property="error", type="string"),
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Internal server error",
     *         @OA\JsonContent(
     *             @OA\Property(property="error", type="string"),
     *         )
     *     )
     * )
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        try {
            $partner = auth()->user();

            $chats = Chat::where('partner_id', $partner->id)->get();

            $chatUrls = [];
            foreach ($chats as $chat) {
                $partnerUrl = url('/chat') . '?token=' . $chat->hash_url . '&user=p' . $chat->partner_id;
                $assistant = $chat->user;

                $chatUrls[] = [
                    'chat_id' => $chat->id,
                    'assistant'=> $assistant,
                    'chat_url' => $partnerUrl,
                ];
            }

            return response()->json(['chats' => $chatUrls], 200);
        } catch (ModelNotFoundException) {
            return $this->handleModelNotFoundException('Partner');
        } catch (Exception) {
            return response()->json(['error' => 'An error occurred while creating chat.'], 500);
        }
    }
}
