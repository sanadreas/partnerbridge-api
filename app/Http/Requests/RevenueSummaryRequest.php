<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RevenueSummaryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'year' => 'required|integer',
            'month' => 'required|integer',
            'revenue' => 'required|numeric',
            'category' => 'nullable|string',
            'product' => 'nullable|string',
            'sales_count' => 'nullable|integer',
            'average_receipt' => 'nullable|numeric',
            'revenue_before_tax' => 'nullable|numeric',
            'tax_amount' => 'nullable|numeric',
            'revenue_after_tax' => 'nullable|numeric',
        ];
    }
}
