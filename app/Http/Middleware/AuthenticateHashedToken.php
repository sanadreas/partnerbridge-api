<?php

namespace App\Http\Middleware;

use App\Models\Chat;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthenticateHashedToken
{
    public function handle(Request $request, Closure $next)
    {
        $token = $request->query('token');
        $user = $request->query('user');

        $userType = substr($user, 0, 1);
        $userId = substr($user, 1);

        $chat = Chat::where('hash_url', $token)->first();

        if (!$chat && !$user) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        $userType === 'a' ? $modelClass = 'App\Models\User' : $modelClass = 'App\Models\Partner';

        $user = $modelClass::find($userId);

        if ($user) {
            Auth::login($user);
        }

        return $next($request);
    }
}
