<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class RevenueSummary extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'year',
        'month',
        'revenue',
        'category',
        'product',
        'sales_count',
        'average_receipt',
        'revenue_before_tax',
        'tax_amount',
        'revenue_after_tax',
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
