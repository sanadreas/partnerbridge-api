<?php

namespace App\Exceptions;

use Illuminate\Http\JsonResponse;

trait HandlesModelNotFoundException
{
    protected function handleModelNotFoundException(string $modelName): JsonResponse
    {
        return response()->json(['error' => "{$modelName} not found"], 404);
    }
}
