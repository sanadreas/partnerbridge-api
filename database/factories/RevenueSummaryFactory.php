<?php

namespace Database\Factories;

use App\Models\RevenueSummary;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\RevenueSummary>
 */
class RevenueSummaryFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    protected $model = RevenueSummary::class;

    public function definition(): array
    {
        return [
            'user_id' => function () {
                return User::factory()->create()->id;
            },
            'year' => $this->faker->year,
            'month' => $this->faker->numberBetween(1, 12),
            'revenue' => $this->faker->randomFloat(2, 100, 10000),
            'category' => $this->faker->word,
            'product' => $this->faker->word,
            'sales_count' => $this->faker->numberBetween(1, 100),
            'average_receipt' => $this->faker->randomFloat(2, 10, 500),
            'revenue_before_tax' => $this->faker->randomFloat(2, 100, 10000),
            'tax_amount' => $this->faker->randomFloat(2, 5, 500),
            'revenue_after_tax' => $this->faker->randomFloat(2, 100, 10000),
        ];
    }
}
