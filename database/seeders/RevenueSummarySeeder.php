<?php

namespace Database\Seeders;

use App\Models\RevenueSummary;
use Illuminate\Database\Seeder;

class RevenueSummarySeeder extends Seeder
{
    public function run()
    {
        for ($i = 1; $i <= 10; $i++) {
            RevenueSummary::create([
                'user_id' => rand(1, 3),
                'year' => rand(2020, 2024),
                'month' => rand(1, 12),
                'revenue' => rand(1000, 10000),
                'category' => 'Категория ' . $i,
                'product' => 'Продукт ' . $i,
                'sales_count' => rand(50, 200),
                'average_receipt' => rand(50, 200),
                'revenue_before_tax' => rand(800, 9000),
                'tax_amount' => rand(50, 1000),
                'revenue_after_tax' => rand(800, 9000),
            ]);
        }
    }
}
