# Запуск приложения с помощью Docker Compose

Этот репозиторий содержит пример настройки приложения Laravel с использованием Docker и Docker Compose.

## Предварительные требования

Прежде чем запустить приложение, убедитесь, что у вас установлены следующие компоненты:

- Docker
- Docker Compose

## Запуск приложения

1. Клонируйте репозиторий:

    ```bash
    git clone https://gitlab.com/sanadreas/partnerbridge-api.git
    ```

2. Перейдите в каталог проекта:

    ```bash
    cd PartnerBridge-API
    ```

3. Создайте файл `.env` и скопируйте в него содержимое файла `.env.example`. Затем внесите необходимые изменения:

    ```bash
    cp .env.example .env
    ```

4. Запустите контейнеры Docker с помощью Docker Compose:

    ```bash
    docker-compose up --build -d
    ```

5. После успешного запуска, откройте ваш браузер и перейдите по следующему URL:

    ```
    http://localhost:8000/api/documentation
    ```

## Остановка приложения

Для остановки приложения выполните следующую команду:

```bash
docker-compose down
